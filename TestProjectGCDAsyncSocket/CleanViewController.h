//
//  CleanViewController.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageFactory.h"
#import "AppModel.h"
#import "WipeAwayView.h"

@interface CleanViewController : UIViewController

@property (strong, nonatomic) AppModel *appModel;

@end
