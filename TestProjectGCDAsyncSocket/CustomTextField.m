//
//  CustomTextField.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 01/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    int marginLeft = 35;
    int marginTop = 7;
    CGRect inset = CGRectMake(bounds.origin.x + marginLeft, bounds.origin.y + marginTop, bounds.size.width - marginLeft, bounds.size.height - marginTop);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    int marginLeft = 35;
    int marginTop = 7;
    CGRect inset = CGRectMake(bounds.origin.x + marginLeft, bounds.origin.y + marginTop, bounds.size.width - marginLeft, bounds.size.height - marginTop);
    return inset;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
