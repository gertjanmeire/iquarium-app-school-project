//
//  WipeAwayView.h
//  WipeAway
//
//  Created by Craig on 12/6/10.
//

#import <UIKit/UIKit.h>
#import "AppModel.h"
#import "CleanCoord.h"

@interface WipeAwayView : UIView {
    
	CGPoint		location;
	CGImageRef	imageRef;
	UIImage		*eraser;
	BOOL		wipingInProgress;
	UIColor		*maskColor;
	CGFloat		eraseSpeed;
	
}

@property (strong, nonatomic) AppModel *appModel;
@property (strong, nonatomic) NSMutableArray *cleanArr;
@property (nonatomic) int counter;

- (void)newMaskWithColor:(UIColor *)color eraseSpeed:(CGFloat)speed;

@end
