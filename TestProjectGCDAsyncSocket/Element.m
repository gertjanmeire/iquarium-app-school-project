//
//  Element.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 14/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "Element.h"

@implementation Element

@synthesize imageName = _imageName;

- (id)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        NSLog(@"Element created");
        self.imageName = name;
    }
    return self;
}

@end
