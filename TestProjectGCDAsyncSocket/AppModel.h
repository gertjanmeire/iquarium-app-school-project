//
//  AppModel.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 28/11/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CleanCoord.h"

@class GCDAsyncSocket;

@interface AppModel : NSObject{
    GCDAsyncSocket *socket;
}

@property (strong, nonatomic) NSString *IP;
@property (strong, nonatomic) GCDAsyncSocket *socket;
@property (strong, nonatomic) NSObject *userInfo;

+(AppModel *)sharedAppModel;
-(void)connectToSocketServer;
-(void)writeToSocket;
-(void)closeConnectionToSocketServer;
-(void)inputProgramJoined:(NSDictionary *)petInfo;
-(void)shakeEventHappened;
-(void)sendCLeaningCoordinates:(NSArray *)cleanCoordsArr;
-(void)turnLightOff:(BOOL)changeLights;
-(void)changeElementPosition:(double)position withOrientation:(NSString *)orientation forElementWithID:(int)elementID;
-(void)addElementToAquarium:(NSString *)elementName;
-(void)startPlayModus;

@end
