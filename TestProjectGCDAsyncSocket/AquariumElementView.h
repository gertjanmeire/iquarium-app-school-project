//
//  AquariumElementView.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 12/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppModel.h"
#import "ImageFactory.h"

@interface AquariumElementView : UIView

@property (strong, nonatomic) AppModel *appModel;

@property (nonatomic) int elementID;
@property (strong, nonatomic) NSNumber *xValue;
@property (strong, nonatomic) NSNumber *yValue;
@property (strong, nonatomic) NSString *imageName;

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *changePositionLabel;
@property (strong, nonatomic) UILabel *xcoordLabel;
@property (strong, nonatomic) UILabel *ycoordLabel;
@property (strong, nonatomic) UISlider *xValueSlider;
@property (strong, nonatomic) UISlider *yValueSlider;

- (id)initWithX:(NSNumber *)xValue andY:(NSNumber *)yValue andID:(NSNumber *)elementID andName:(NSString *)elementName;

@end
