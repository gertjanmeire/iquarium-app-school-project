//
//  CleanCoord.h
//  iQuarium
//
//  Created by Gert-Jan Meire on 17/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CleanCoord : NSObject

@property (strong, nonatomic) NSNumber *xCoord;
@property (strong, nonatomic) NSNumber *yCoord;

- (id)initWithXCoord:(NSNumber *)xCoord andYCoord:(NSNumber *)yCoord;

@end
