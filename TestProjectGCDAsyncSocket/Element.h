//
//  Element.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 14/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Element : NSObject

@property (strong, nonatomic) NSString *imageName;

- (id)initWithName:(NSString *)name;

@end
