//
//  AppModel.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 28/11/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "AppModel.h"
#import "GCDAsyncSocket.h"
#import "DDLog.h"
#import "DDTTYLogger.h"


@implementation AppModel

@synthesize IP = _IP;
@synthesize socket = socket;
@synthesize userInfo = _userInfo;

/**********************************************************************************/
// Singleton constructor
/**********************************************************************************/
+(AppModel *)sharedAppModel{
    static AppModel *sharedAppModel;
    
    @synchronized(self){
        if(!sharedAppModel){
            sharedAppModel = [[AppModel alloc] init];
            sharedAppModel.IP = @"192.168.1.4";
        }
      
        return sharedAppModel;
    }
}

/**********************************************************************************/
// Connect to the socket server on the IP address of the server
/**********************************************************************************/
-(void)connectToSocketServer{
    //Connect to the socketserver
    NSError *err = nil;
    socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    if(![socket connectToHost:self.IP onPort:1234 error:&err]){
        NSLog(@"Error %@", err);
    }
}

/**********************************************************************************/
// Catch a GCDAsyncSocket connection event
// If connected send a notification "CONNECTED" (caught in MainViewController
/**********************************************************************************/
- (void)socket:(GCDAsyncSocket *)sender didConnectToHost:(NSString *)host port:(UInt16)port
{
    //When connected send a CONNECTED notification to be caught in MainViewController
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CONNECTED" object:nil];
}

/**********************************************************************************/
// Send a JSON object to the server to tell an input program has joined
/**********************************************************************************/
-(void)inputProgramJoined:(NSDictionary *)petInfo{
    NSObject *userInfoObject = self.userInfo;

    NSDictionary *json = [[NSDictionary alloc] initWithObjectsAndKeys:@"INPUT_PROGRAM_JOIN", @"command", userInfoObject, @"userInfo", petInfo, @"petInfo", nil];

    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&writeError];
        
    [socket writeData:jsonData withTimeout:-1 tag:1];
}

/**********************************************************************************/
// Send a JSON object to the server to tell an shake has happened (feed the pet)
/**********************************************************************************/
-(void)shakeEventHappened{
    NSLog(@"Sending shake event to server");
    NSDictionary *json = [[NSDictionary alloc] initWithObjectsAndKeys:@"FEED", @"command", nil];
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&writeError];
    [socket writeData:jsonData withTimeout:-1 tag:1];
}

/**********************************************************************************/
// Send a JSON object to the server to tell the cleaning coordinates
/**********************************************************************************/
-(void)sendCLeaningCoordinates:(NSArray *)cleanCoordsArr{
    NSLog(@"Sending cleaning coordinates to server");
    
    //NSArray *coords = [NSArray arrayWithObjects:[NSNumber numberWithDouble:xCoord], [NSNumber numberWithDouble:yCoord], nil];
    
    NSDictionary *json = [[NSDictionary alloc] initWithObjectsAndKeys:@"CLEAN", @"command", cleanCoordsArr, @"data", nil];
    
    if( [NSJSONSerialization isValidJSONObject:json]  ){
        NSLog(@"VALID JSON");
        NSError *writeError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&writeError];
        
        [socket writeData:jsonData withTimeout:-1 tag:1];
        
    }else{
        NSLog(@"NOT VALID JSON");
    }
    

}

/**********************************************************************************/
// Send a JSON object to the server to tell to change the lights 
/**********************************************************************************/
-(void)turnLightOff:(BOOL)changeLights{
    NSLog(@"Sending turn light on or off to server");
    
    NSString *val;
    
    BOOL change = changeLights;
    if(change == YES){
        val = @"YES";
    }else{
        val = @"NO";
    }
    
    NSArray *value = [NSArray arrayWithObject:val];
    
    NSDictionary *json = [[NSDictionary alloc] initWithObjectsAndKeys:@"SLEEP", @"command", value, @"data", nil];
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&writeError];
    [socket writeData:jsonData withTimeout:-1 tag:1];
}

/**********************************************************************************/
// Send a JSON object to the server to change the position of an element 
/**********************************************************************************/
-(void)changeElementPosition:(double)position withOrientation:(NSString *)orientation forElementWithID:(int)elementID{
    NSLog(@"Inside AppModel change position");
    NSArray *elementInformation = [NSArray arrayWithObjects:[NSNumber numberWithDouble:position], orientation, [NSNumber numberWithInt:elementID], nil];
    
    NSDictionary *json = [[NSDictionary alloc] initWithObjectsAndKeys:@"CHANGE_POSITION", @"command", elementInformation, @"data", nil];
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&writeError];
    [socket writeData:jsonData withTimeout:-1 tag:1];
}

/**********************************************************************************/
// Send a JSON object to the server to add an element to the aquarium
/**********************************************************************************/
-(void)addElementToAquarium:(NSString *)elementName{
    NSLog(@"Inside AppModel add element to aquarium: %@", elementName);
    NSDictionary *json = [[NSDictionary alloc] initWithObjectsAndKeys:@"ADD_ELEMENT", @"command", elementName, @"data", nil];
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&writeError];
    [socket writeData:jsonData withTimeout:-1 tag:1];
}

/**********************************************************************************/
// Send a JSON object to the server to start the play modus
/**********************************************************************************/
-(void)startPlayModus{
    NSLog(@"Inside AppModel start play modus");
    
    NSDictionary *json = [[NSDictionary alloc] initWithObjectsAndKeys:@"PLAY", @"command", nil];
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&writeError];
    [socket writeData:jsonData withTimeout:-1 tag:1];
}

/**********************************************************************************/
// Method that handles the reading of incoming data over GCDAsyncSocket
/**********************************************************************************/
- (void)socket:(GCDAsyncSocket *)sender didReadData:(NSData *)data withTag:(long)tag
{
    NSLog(@"Received data");
    NSLog(@"%@", data);
}

/**********************************************************************************/
// Disconnect from the server
/**********************************************************************************/
-(void)closeConnectionToSocketServer{
    [socket disconnect];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DISCONNECTED" object:nil];
}


@end
