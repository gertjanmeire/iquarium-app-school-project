//
//  MainViewController.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 27/11/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "MainViewController.h"
#import "ImageFactory.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize appModel = appModel;
@synthesize usernameField = _usernameField;
@synthesize passwordField = _passwordField;
@synthesize loginButton = _loginButton;
@synthesize tabbarLoginButton = _tabbarLoginButton;
@synthesize tabbarRegistrationButton = _tabbarRegistrationButton;
@synthesize loginView = _loginView;
@synthesize registrationView = _registrationView;
@synthesize errorImageView = _errorImageView;
@synthesize errorRegistrationImageView = _errorRegistrationImageView;
@synthesize registeredView = _registeredView;
@synthesize registeredText = _registeredText;
@synthesize gotoLoginButton = _gotoLoginButton;
@synthesize activityIndactor = _activityIndactor;

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        NSLog(@"Main view controller initialised");
        
        //Set app model
        appModel = [[AppModel sharedAppModel] init];
        
    }
    return self;
}

-(void)checkIfUserExists:(UIButton *)sender{
    NSLog(@"test");
    if(sender == self.loginButton){
        NSString *username = self.usernameField.text;
        NSString *pass = self.passwordField.text;
        NSArray *loginArguments = [NSArray arrayWithObjects:username, pass, nil];
        NSLog(@"username: %@", username);
        //Check webservice is user exists
        //If so change view else show error message
        NSString *URLString = [NSString stringWithFormat:@"http://%@:8888/amfphp-2.1/Amfphp/", self.appModel.IP];
        AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"ExDevService" method:@"checkIfUserExists" arguments:[NSArray arrayWithObjects: loginArguments, nil]];
        m_remotingCall.delegate = self;
        [m_remotingCall start];
    }
}

- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSObject *)object
{
	NSLog(@"[MainVC] remoting call was successful. received data: %@", [object valueForKey:@"validation"]);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    NSString *validationString = (NSString *)[object valueForKey:@"validation"];
    NSString *userExistsString = (NSString *)[object valueForKey:@"userExists"];
    
    if([validationString isEqualToString:@"login"]){
        NSLog(@"Login event came back from AMFPHP");
        
        if([userExistsString isEqualToString:@"yes"]){
            //Log the user in
            //Add the user his info to the AppModel
            appModel.userInfo = object;
            
            //Listen for CONNECTED event comming from AppModel to see if the user is connected to the socket server
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectedHandler:) name:@"CONNECTED" object:nil];
            
            //Connect the user to the socket server
            [appModel connectToSocketServer];
        }else{
            //Show error that the user does not exists
            UIImage *loginErrorImage = [ImageFactory createImageWithName:@"loginError" andType:@"png" andDirectory:@"images"];
            self.errorImageView = [[UIImageView  alloc] initWithImage:loginErrorImage];
            [self.errorImageView setFrame:CGRectMake(0, 0, loginErrorImage.size.width, loginErrorImage.size.height)];
            [self.view addSubview:self.errorImageView];
        }        
    }else if([validationString isEqualToString:@"registration"]){
        NSLog(@"Registration event came back from AMFPHP");
        //Hide the error from not filling in all fields
        if(self.errorImageView.hidden == NO){
            self.errorImageView.hidden = YES;
        }
        
        NSString *errorString = (NSString *)[object valueForKey:@"error"];
        
        if([errorString isEqualToString:@"yes"]){
            //Show error that user did already exist
            UIImage *registrationErrorImage = [ImageFactory createImageWithName:@"userAlreadyExistsError" andType:@"png" andDirectory:@"images"];
            self.errorImageView = [[UIImageView  alloc] initWithImage:registrationErrorImage];
            [self.errorImageView setFrame:CGRectMake(0, 0, registrationErrorImage.size.width, registrationErrorImage.size.height)];
            [self.view addSubview:self.errorImageView];
        }else{
            //Show to registered view
            self.registrationView.hidden = YES;
            self.registeredView.hidden = NO;
        }        
    }
}

- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"[MainVC] remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}

//If the user is connected "AppModel" will send a notification named "CONNECTED"
//Push the menu view on the stack then
-(void)connectedHandler:(NSNotification *)notification{
    NSLog(@"[MainVC] CONNECTED TO SERVER");
    SelectPetViewController *selectPetVC = [[SelectPetViewController alloc] init];
    [self.navigationController pushViewController:selectPetVC animated:YES];
}

-(void)saveUser:(UIButton *)sender{
    NSLog(@"[MainVC] Saving user to DB");
    if(sender == self.registrationButton){
        NSString *name = self.regnameField.text;
        NSString *username = self.regusernameField.text;
        NSString *pass = self.regpasswordField.text;
        
        //Check if all fields are filled in correctly
        if([name isEqualToString:@""] || [username isEqualToString:@""] || [pass isEqualToString:@""]){
            //Show error label
            UIImage *registrationErrorImage = [ImageFactory createImageWithName:@"registrationError" andType:@"png" andDirectory:@"images"];
            self.errorImageView = [[UIImageView  alloc] initWithImage:registrationErrorImage];
            [self.errorImageView setFrame:CGRectMake(0, 0, registrationErrorImage.size.width, registrationErrorImage.size.height)];
            [self.view addSubview:self.errorImageView];
        }else{
            //Save the user to the DB via AMFPHP
            NSArray *userInfoArguments = [NSArray arrayWithObjects:name, username, pass, nil];
            
            //Save user through webservice
            //If so change view else show error message
            NSString *URLString = [NSString stringWithFormat:@"http://%@:8888/amfphp-2.1/Amfphp/", self.appModel.IP];
            AMFRemotingCall *saveUserToDBCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"ExDevService" method:@"saveUserToDB" arguments:[NSArray arrayWithObjects: userInfoArguments, nil]];
            [saveUserToDBCall setDelegate:self];
            [saveUserToDBCall start];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"[MainVC] View did load - %@", self.navigationController);
    
    //Set background image
    
    //NSString *backgroundPath = [[NSBundle mainBundle] pathForResource:@"bg" ofType:@"png" inDirectory:@"images"];
    //NSLog(@"Background path = %@", backgroundPath);
    //UIImage *backgroundImage = [UIImage imageNamedForDevice:backgroundPath];
    
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    
    //Set logo
    UIImage *logoImage = [ImageFactory createImageWithName:@"logo" andType:@"png" andDirectory:@"images"];
    UIImageView *logo = [[UIImageView alloc] initWithImage:logoImage];
    logo.frame = CGRectMake(0, 0, logoImage.size.width, logoImage.size.height);
    [logo setFrame:CGRectMake(104, 25, logoImage.size.width, logoImage.size.height)];
    [self.view addSubview:logo];
    
    
    //self.activityIndactor = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //[self.view addSubview:self.activityIndactor];
        
    [self.navigationItem setHidesBackButton:YES];
    
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_login" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    
    //Create 2 UIViews to hold login and registration
    //LOGIN VIEW
    self.loginView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.view addSubview:self.loginView];
    
    //Username textfield
    UIImage *usernameFieldImage = [ImageFactory createImageWithName:@"usernameField" andType:@"png" andDirectory:@"images"];
    self.usernameField = [[CustomTextField alloc] initWithFrame:CGRectMake(32, 125, usernameFieldImage.size.width, usernameFieldImage.size.height)];
    [self.usernameField setBackground:usernameFieldImage];
    [self.usernameField setText:@"gertjanmeire"];
    [self.usernameField setDelegate:self];
    [self.loginView addSubview:self.usernameField];
    
    //Password textfield
    UIImage *passwordFieldImage = [ImageFactory createImageWithName:@"passwordField" andType:@"png" andDirectory:@"images"];
    self.passwordField = [[CustomTextField alloc] initWithFrame:CGRectMake(32, 175, passwordFieldImage.size.width, passwordFieldImage.size.height)];
    [self.passwordField setBackground:passwordFieldImage];
    [self.passwordField setText:@"adminpass"];
    [self.passwordField setDelegate:self];
    [self.loginView addSubview:self.passwordField];
    
    //Login button
    UIImage *loginButtonImage = [ImageFactory createImageWithName:@"loginButton" andType:@"png" andDirectory:@"images"];
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setImage:loginButtonImage forState:UIControlStateNormal];
    self.loginButton.frame = CGRectMake(32, 225, loginButtonImage.size.width, loginButtonImage.size.height);
    [self.loginButton addTarget:self action:@selector(checkIfUserExists:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView addSubview:self.loginButton];
        
    UIImage *tabbarLoginButtonImage = [ImageFactory createImageWithName:@"tabbarLoginButtonHighlighted" andType:@"png" andDirectory:@"images"];
    self.tabbarLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tabbarLoginButton.frame = CGRectMake(0, 455, tabbarLoginButtonImage.size.width, tabbarLoginButtonImage.size.height);
    [self.tabbarLoginButton setImage:tabbarLoginButtonImage forState:UIControlStateNormal];
    [self.loginView addSubview:self.tabbarLoginButton];
    
    UIImage *tabbarRegistrationButtonImage = [ImageFactory createImageWithName:@"tabbarRegistrationButton" andType:@"png" andDirectory:@"images"];
    self.tabbarRegistrationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tabbarRegistrationButton.frame = CGRectMake(160, 455, tabbarRegistrationButtonImage.size.width, tabbarRegistrationButtonImage.size.height);
    [self.tabbarRegistrationButton setImage:tabbarRegistrationButtonImage forState:UIControlStateNormal];
    [self.tabbarRegistrationButton addTarget:self action:@selector(tabbarHandler:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView addSubview:self.tabbarRegistrationButton];

    //REGISTRATION VIEW
    self.registrationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.registrationView.hidden = YES;
    [self.view addSubview:self.registrationView];
    
    //Name textfield
    UIImage *regnameFieldImage = [ImageFactory createImageWithName:@"usernameField" andType:@"png" andDirectory:@"images"];
    self.regnameField = [[CustomTextField alloc] initWithFrame:CGRectMake(32, 125, regnameFieldImage.size.width, regnameFieldImage.size.height)];
    [self.regnameField setBackground:regnameFieldImage];
    [self.regnameField setText:@""];
    [self.regnameField setDelegate:self];
    [self.registrationView addSubview:self.regnameField];
    
    
    //Username textfield
    UIImage *regusernameFieldImage = [ImageFactory createImageWithName:@"usernameField" andType:@"png" andDirectory:@"images"];
    self.regusernameField = [[CustomTextField alloc] initWithFrame:CGRectMake(32, 175, regusernameFieldImage.size.width, regusernameFieldImage.size.height)];
    [self.regusernameField setBackground:regusernameFieldImage];
    [self.regusernameField setText:@""];
    [self.regusernameField setDelegate:self];
    [self.registrationView addSubview:self.regusernameField];
    
    
    //Password textfield
    UIImage *regpasswordFieldImage = [ImageFactory createImageWithName:@"passwordField" andType:@"png" andDirectory:@"images"];
    self.regpasswordField = [[CustomTextField alloc] initWithFrame:CGRectMake(32, 225, regpasswordFieldImage.size.width, regpasswordFieldImage.size.height)];
    [self.regpasswordField setBackground:regpasswordFieldImage];
    [self.regpasswordField setText:@""];
    [self.regpasswordField setDelegate:self];
    [self.registrationView addSubview:self.regpasswordField];
    
    //Registration button
    UIImage *registrationButtonImage = [ImageFactory createImageWithName:@"registrationButton" andType:@"png" andDirectory:@"images"];
    self.registrationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registrationButton setImage:registrationButtonImage forState:UIControlStateNormal];
    self.registrationButton.frame = CGRectMake(32, 275, registrationButtonImage.size.width, registrationButtonImage.size.height);
    [self.registrationButton addTarget:self action:@selector(saveUser:) forControlEvents:UIControlEventTouchUpInside];
    [self.registrationView addSubview:self.registrationButton];
    
    UIImage *regtabbarLoginButtonImage = [ImageFactory createImageWithName:@"tabbarLoginButton" andType:@"png" andDirectory:@"images"];
    self.regtabbarLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.regtabbarLoginButton.frame = CGRectMake(0, 455, regtabbarLoginButtonImage.size.width, regtabbarLoginButtonImage.size.height);
    [self.regtabbarLoginButton setImage:regtabbarLoginButtonImage forState:UIControlStateNormal];
    [self.regtabbarLoginButton addTarget:self action:@selector(tabbarHandler:) forControlEvents:UIControlEventTouchUpInside];
    [self.registrationView addSubview:self.regtabbarLoginButton];
    
    UIImage *regtabbarRegistrationButtonImage = [ImageFactory createImageWithName:@"tabbarRegistrationButtonHighlighted" andType:@"png" andDirectory:@"images"];
    self.regtabbarRegistrationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.regtabbarRegistrationButton.frame = CGRectMake(160, 455, regtabbarRegistrationButtonImage.size.width, regtabbarRegistrationButtonImage.size.height);
    [self.regtabbarRegistrationButton setImage:regtabbarRegistrationButtonImage forState:UIControlStateNormal];
    [self.registrationView addSubview:self.regtabbarRegistrationButton];
    
    
    self.registeredView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.registeredView.hidden = YES;
    [self.view addSubview:self.registeredView];
    
    self.registeredText = [ImageFactory createImageWithName:@"registeredText" andType:@"png" andDirectory:@"images"];
    UIImageView *registeredTextView = [[UIImageView alloc] initWithImage:self.registeredText];
    [registeredTextView setFrame:CGRectMake(38, 153, self.registeredText.size.width, self.registeredText.size.height)];
    [self.registeredView addSubview:registeredTextView];
    
    UIImage *gotoLoginButtonImage = [ImageFactory createImageWithName:@"gotoLoginButton" andType:@"png" andDirectory:@"images"];
    self.gotoLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.gotoLoginButton setImage:gotoLoginButtonImage forState:UIControlStateNormal];
    self.gotoLoginButton.frame = CGRectMake(105, 220, gotoLoginButtonImage.size.width, gotoLoginButtonImage.size.height);
    [self.gotoLoginButton addTarget:self action:@selector(gotoLoginView:) forControlEvents:UIControlEventTouchUpInside];
    [self.registeredView addSubview:self.gotoLoginButton];

    
}

-(void)gotoLoginView:(UIButton *)sender{
    if(sender == self.gotoLoginButton){
        self.registeredView.hidden = YES;
        self.loginView.hidden = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_login" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    [super viewWillAppear:animated];
}

-(void)tabbarHandler:(UIButton *)sender{
    NSLog(@"[MainVC] tabbar handler");
    if(self.registrationView.hidden == YES){
        UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_registration" andType:@"png" andDirectory:@"images"];
        [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
        self.registrationView.hidden = NO;
        self.loginView.hidden = YES;
    }else{
        UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_login" andType:@"png" andDirectory:@"images"];
        [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
        self.loginView.hidden = NO;
        self.registrationView.hidden = YES;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
