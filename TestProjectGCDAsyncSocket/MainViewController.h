//
//  MainViewController.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 27/11/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMFRemotingCall.h"
#import "AppModel.h"
#import "SelectPetViewController.h"
#import "CustomTextField.h"


@interface MainViewController : UIViewController <AMFRemotingCallDelegate, UITextFieldDelegate>
{
    AppModel *appModel;
}

@property (strong, nonatomic) AppModel *appModel;
@property (strong, nonatomic) UIView *loginView;
@property (strong, nonatomic) CustomTextField *usernameField;
@property (strong, nonatomic) CustomTextField *passwordField;
@property (strong, nonatomic) UIButton *loginButton;

@property (strong, nonatomic) UIView *registrationView;
@property (strong, nonatomic) CustomTextField *regnameField;
@property (strong, nonatomic) CustomTextField *regusernameField;
@property (strong, nonatomic) CustomTextField *regpasswordField;
@property (strong, nonatomic) UIButton *registrationButton;
@property (strong, nonatomic) UIImageView *errorImageView;
@property (strong, nonatomic) UIImageView *errorRegistrationImageView;

@property (strong, nonatomic) UIView *registeredView;


@property (strong, nonatomic) UIButton *tabbarLoginButton;
@property (strong, nonatomic) UIButton *tabbarRegistrationButton;
@property (strong, nonatomic) UIButton *regtabbarLoginButton;
@property (strong, nonatomic) UIButton *regtabbarRegistrationButton;


@property (strong, nonatomic) UIActivityIndicatorView *activityIndactor;
@property (strong, nonatomic) UIImage *registeredText;
@property (strong, nonatomic) UIButton *gotoLoginButton;



@end
