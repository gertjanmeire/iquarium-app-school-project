//
//  PetView.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageFactory.h"

@interface PetView : UIView

@property (nonatomic) NSNumber *petID;
@property (strong, nonatomic) NSString *mood;
@property (strong, nonatomic) NSString *name;
@property (nonatomic) NSNumber *state;
@property (nonatomic) NSNumber *type;
@property (nonatomic) NSNumber *hp;
@property (nonatomic, strong) NSNumber *time_since_eaten;
@property (nonatomic) NSNumber *userID;

- (id)initWithID:(NSNumber *)petID andMood:(NSString *)mood andName:(NSString *)name andState:(NSNumber *)state andType:(NSNumber *)type andPetHP:(NSNumber *)petHP andPetTimeSinceEaten:(NSNumber *)petTimeSinceEaten andUserID:(NSNumber *)userID;

@end
