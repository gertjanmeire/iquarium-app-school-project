//
//  OrganizeViewController.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 10/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMFRemotingCall.h"
#import "ImageFactory.h"
#import "AppModel.h"
#import "AquariumElementView.h"
#import "Element.h"

@interface OrganizeViewController : UIViewController <AMFRemotingCallDelegate>

@property (strong, nonatomic) AppModel *appModel;

@property (strong, nonatomic) UIView *organizeView;
@property (strong, nonatomic) UIScrollView *decorationScrollView;
@property (strong, nonatomic) UIButton *addElementButton;


@property (strong, nonatomic) UIView *addElementView;
@property (strong, nonatomic) UIButton *backToOrganizeButton;
@property (strong, nonatomic) UIView *addElementSubView;
@property (strong, nonatomic) UIScrollView *elementsScrollview;
@property (strong, nonatomic) UIButton *addNewElementButton;




@end
