//
//  PetTypeView.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 08/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "PetTypeView.h"

@implementation PetTypeView

- (id)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        // Initialization code
        NSLog(@"Pet type view initialised");
        self.name = name;
        
        if(self.name == @"piranha"){
            UIImage *pettypeImage = [ImageFactory createImageWithName:@"createPetType1" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:pettypeImage];
            [imageView setFrame:CGRectMake(75, 30, pettypeImage.size.width, pettypeImage.size.height)];
            [self addSubview:imageView];
        }else if(self.name == @"fish"){
            UIImage *optionImage = [ImageFactory createImageWithName:@"createPetType2" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:optionImage];
            [imageView setFrame:CGRectMake(75, 30, optionImage.size.width, optionImage.size.height)];
            [self addSubview:imageView];
        }
    }
    return self;
}


@end
