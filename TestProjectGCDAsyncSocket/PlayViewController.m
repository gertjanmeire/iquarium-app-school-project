//
//  PlayViewController.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "PlayViewController.h"

@interface PlayViewController ()

@end

@implementation PlayViewController

@synthesize appModel = _appModel;
@synthesize playButton = _playButton;
@synthesize stopButton = _stopButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.appModel = [AppModel sharedAppModel];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationItem setHidesBackButton:YES];
    
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg_playing" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_play" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    UIImage *playButtonImage = [ImageFactory createImageWithName:@"playButton" andType:@"png" andDirectory:@"images"];
    self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.playButton setImage:playButtonImage forState:UIControlStateNormal];
    self.playButton.frame = CGRectMake(30, 200, playButtonImage.size.width, playButtonImage.size.height);
    [self.playButton addTarget:self action:@selector(playButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.playButton];
    
    UIImage *stopButtonImage = [ImageFactory createImageWithName:@"stopButton" andType:@"png" andDirectory:@"images"];
    self.stopButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.stopButton setImage:stopButtonImage forState:UIControlStateNormal];
    self.stopButton.frame = CGRectMake(30, 200, stopButtonImage.size.width, stopButtonImage.size.height);
    [self.stopButton addTarget:self action:@selector(playButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.stopButton];
    self.stopButton.hidden = YES;
    
    
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem = navbarBackButton;

}

-(void)backButton:(UIButton *)sender{    
    //Change navigationcontroller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)playButton:(UIButton *)sender{
    if(self.stopButton.hidden == YES){
        self.stopButton.hidden = NO;
        self.playButton.hidden = YES;
    }else{
        self.stopButton.hidden = YES;
        self.playButton.hidden = NO;
    }
    [self.appModel startPlayModus];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
