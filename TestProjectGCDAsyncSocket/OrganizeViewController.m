//
//  OrganizeViewController.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 10/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "OrganizeViewController.h"

@interface OrganizeViewController ()

@end

@implementation OrganizeViewController

@synthesize appModel = _appModel;

@synthesize organizeView = _organizeView;
@synthesize addElementButton = _addElementButton;
@synthesize decorationScrollView = _decorationScrollView;

@synthesize addElementView = _addElementView;
@synthesize backToOrganizeButton = _backToOrganizeButton;
@synthesize addElementSubView = _addElementSubView;
@synthesize elementsScrollview = _elementsScrollview;
@synthesize addNewElementButton = _addNewElementButton;


- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        NSLog(@"OrganizeViewController");
        self.appModel = [AppModel sharedAppModel];

        [self setup];
        
    }
    
    return self;
}

-(void)setup{
    //Get the user info from the AppModel
    NSObject *userInfoObject = self.appModel.userInfo;
    
    //Get the string containing the JSON object from the user info
    NSString *aquariumDescriptionForUser = [userInfoObject valueForKey:@"aquarium_description"];
    
    NSLog(@"%@", aquariumDescriptionForUser);
    
    //Turn this string into NSData object
    NSData *data = [aquariumDescriptionForUser dataUsingEncoding:NSUnicodeStringEncoding];
    
    //Get the array from the NSData object
    NSError *error = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    //Get the array in the JSON string
    NSArray *items = [jsonArray valueForKey:@"plants"];
    
    
    //Check if the scrollview is empty
    //If not empty it and rebuild it's content
    for (AquariumElementView * aquariumElementView in self.decorationScrollView.subviews) {
        NSLog(@"Remove aquarium subview from scrollview");
        [aquariumElementView removeFromSuperview];
    }
    
    NSInteger elementsCount= items.count;
    int i = 0;
    //For every dictionary in this array get it's items and create the interface for it
    for(NSDictionary *item in items){
        NSLog(@"test: %f", self.view.frame.size.height - 127);
        CGFloat y = i * 421;
        NSLog(@"y %f", y);
        
        AquariumElementView *aquariumElementView = [[AquariumElementView alloc] initWithX:(NSNumber *)[item valueForKey:@"x"] andY:(NSNumber *)[item valueForKey:@"x"] andID:(NSNumber *)[item valueForKey:@"id"] andName:(NSString *)[item valueForKey:@"image"]];
        [aquariumElementView setFrame:CGRectMake(0, y+5, 259, 416)];;
        [self.decorationScrollView addSubview:aquariumElementView];
        
        i++;
    }
    
    self.decorationScrollView.contentSize = CGSizeMake(259, 421 *elementsCount);
    
    
    //NSDictionary for the elements the user can add
    NSArray *elementsArr = [NSArray arrayWithObjects:[[Element alloc] initWithName:@"plant1"], [[Element alloc] initWithName:@"plant2"],[[Element alloc] initWithName:@"plant3"],[[Element alloc] initWithName:@"plant4"], [[Element alloc] initWithName:@"plant5"], [[Element alloc] initWithName:@"plant6"], [[Element alloc] initWithName:@"plant7"], [[Element alloc] initWithName:@"plant8"], [[Element alloc] initWithName:@"rock1"], [[Element alloc] initWithName:@"rock2"], nil];
    
    NSInteger numberOfElements= elementsArr.count;
    i = 0;
    //For every Element in this array get it's items and create the interface for it
    for(Element *element in elementsArr){
        NSLog(@"test: %f", self.view.frame.size.height - 127);
        CGFloat x = i * self.addElementSubView.bounds.size.width;
        NSLog(@"y %f", x);
        
        UIImage *imageForView = [ImageFactory createImageWithName:(NSString *)[element valueForKey:@"imageName"] andType:@"png" andDirectory:@"images"];
        UIImageView *elementView = [[UIImageView alloc] initWithImage:imageForView];
        [elementView setFrame:CGRectMake(x+5, 0, 259, 234)];
        [self.elementsScrollview addSubview:elementView];
        
        i++;
    }
    
    self.elementsScrollview.contentSize = CGSizeMake(self.addElementSubView.bounds.size.width * numberOfElements, 234);

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationItem setHidesBackButton:YES];
    
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem = navbarBackButton;
    
    self.organizeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [self.view addSubview:self.organizeView];
    
    //Add element button
    UIImage *addElementButtonImage = [ImageFactory createImageWithName:@"addElementButton" andType:@"png" andDirectory:@"images"];
    self.addElementButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addElementButton setImage:addElementButtonImage forState:UIControlStateNormal];
    [self.addElementButton setFrame:CGRectMake(30, 10, addElementButtonImage.size.width, addElementButtonImage.size.height)];
    [self.addElementButton addTarget:self action:@selector(addElement:) forControlEvents:UIControlEventTouchUpInside];
    [self.organizeView addSubview:self.addElementButton];

    //Create a scrollview for all the items in the aquarium
    //Set the UIScrollView with all the user's pet's
    UIImage *elementPickerImage = [ImageFactory createImageWithName:@"elementPicker" andType:@"png" andDirectory:@"images"];
    self.decorationScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(30, 55, elementPickerImage.size.width, elementPickerImage.size.height)];
    self.decorationScrollView.backgroundColor = [UIColor colorWithPatternImage: elementPickerImage];
    [self.decorationScrollView setBounces:YES];
    [self.decorationScrollView setPagingEnabled:YES];
    [self.decorationScrollView setShowsVerticalScrollIndicator:YES];
    [self.decorationScrollView setCanCancelContentTouches:NO];
    
    self.decorationScrollView.delegate = self;
    [self.organizeView addSubview:self.decorationScrollView];
    
    
    
    //Add element stuff
    self.addElementView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.addElementView.hidden = YES;
    [self.view addSubview:self.addElementView];
    
    //Back to organize button
    UIImage *backToOrganizeButtonImage = [ImageFactory createImageWithName:@"backToOrganizeButton" andType:@"png" andDirectory:@"images"];
    self.backToOrganizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backToOrganizeButton setImage:backToOrganizeButtonImage forState:UIControlStateNormal];
    [self.backToOrganizeButton setFrame:CGRectMake(30, 10, backToOrganizeButtonImage.size.width, backToOrganizeButtonImage.size.height)];
    [self.backToOrganizeButton addTarget:self action:@selector(addElement:) forControlEvents:UIControlEventTouchUpInside];
    [self.addElementView addSubview:self.backToOrganizeButton];
    
    UIImage *addElementSubViewImage = [ImageFactory createImageWithName:@"addElementSubView" andType:@"png" andDirectory:@"images"];
    self.addElementSubView = [[UIView alloc] initWithFrame:CGRectMake(30, self.backToOrganizeButton.bounds.size.height + 20, addElementSubViewImage.size.width, addElementSubViewImage.size.height)];
    [self.addElementSubView setBackgroundColor:[UIColor colorWithPatternImage:addElementSubViewImage]];
    [self.addElementView addSubview:self.addElementSubView];
    
    //Create a scrollview for all the elements a user can add
    self.elementsScrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 10, self.addElementSubView.bounds.size.width, 234)];
    [self.elementsScrollview setBounces:YES];
    [self.elementsScrollview setPagingEnabled:YES];
    [self.elementsScrollview setShowsVerticalScrollIndicator:YES];
    [self.elementsScrollview setCanCancelContentTouches:NO];
    
    self.elementsScrollview.delegate = self;
    [self.addElementSubView addSubview:self.elementsScrollview];
    
    //Add element button
    UIImage *addElementImage = [ImageFactory createImageWithName:@"addElementSubmitButton" andType:@"png" andDirectory:@"images"];
    self.addNewElementButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addNewElementButton setImage:addElementImage forState:UIControlStateNormal];
    [self.addNewElementButton setFrame:CGRectMake(15, 265, addElementImage.size.width, addElementImage.size.height)];
    [self.addNewElementButton addTarget:self action:@selector(addNewElementToAquarium:) forControlEvents:UIControlEventTouchUpInside];
    [self.addElementSubView addSubview:self.addNewElementButton];



}

-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addElement:(UIButton *)sender{
    NSLog(@"Add element button clicked");
    //Show add element screen
    if(self.addElementView.hidden == YES){
        NSLog(@"Show add elementview");
        self.addElementView.hidden = NO;
        self.organizeView.hidden = YES;
    }else{
        NSLog(@"Show organize view");
        self.organizeView.hidden = NO;
        self.addElementView.hidden = YES;
        
        NSLog(@"Going to update user info");
        [self updateUserInfo];
    }
}

-(void)addNewElementToAquarium:(UIButton *)sender{
    NSLog(@"Add this element to your aquarium");
    int optionIndex = (int)(self.elementsScrollview.contentOffset.x / self.elementsScrollview.frame.size.width);
    NSLog(@"Index of option: %d", optionIndex);
    
    NSString *elementName = @"";
    
    if(optionIndex == 0){
        //Add element to aquarium and go back to organize view
        elementName = @"plant1";
    }else if(optionIndex == 1){
        elementName = @"plant2";
    }else if(optionIndex == 2){
        elementName = @"plant3";
    }else if(optionIndex == 3){
        elementName = @"plant4";
    }else if(optionIndex == 4){
        elementName = @"plant5";
    }else if(optionIndex == 5){
        elementName = @"plant6";
    }else if(optionIndex == 6){
        elementName = @"plant7";
    }else if(optionIndex == 7){
        elementName = @"plant8";
    }else if(optionIndex == 8){
        elementName = @"rock1";
    }else if(optionIndex == 9){
        elementName = @"rock2";        
    }
    
    NSLog(@"Going to add element: %@", elementName);
    [self.appModel addElementToAquarium:elementName];
    
    //Go back to the organize view
    //[self addElement:nil];
}


- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES];
    
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_organize" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
        
    [super viewWillAppear:animated];
}

-(void)updateUserInfo{
    NSLog(@"Update user info function");
    NSString *username = [self.appModel.userInfo valueForKey:@"username"];
    
    NSArray *updateArguments = [NSArray arrayWithObjects:username, nil];
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"http://%@:8888/amfphp-2.1/Amfphp/", self.appModel.IP];
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"ExDevService" method:@"getUserInfo" arguments:[NSArray arrayWithObjects: updateArguments, nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
}

- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSObject *)object
{
	NSLog(@"[MainVC] remoting call was successful. received data: %@", [object valueForKey:@"validation"]);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    NSString *validationString = (NSString *)[object valueForKey:@"validation"];
    
    if([validationString isEqualToString:@"updateUserInfo"]){
        NSLog(@"Update user info event came back from AMFPHP");
        
            //Add the user his info to the AppModel
            self.appModel.userInfo = object;
        
        [self setup];
    }
}

- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"[MainVC] remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
