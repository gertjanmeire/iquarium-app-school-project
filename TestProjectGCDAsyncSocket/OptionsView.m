//
//  OptionsView.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "OptionsView.h"

@implementation OptionsView

@synthesize name = _name;

- (id)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        // Initialization code
        self.name = name;
                
        UIImage *petImage = [ImageFactory createImageWithName:@"petType1" andType:@"png" andDirectory:@"images"];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:petImage];
        [imageView setFrame:CGRectMake(100, 20, petImage.size.width, petImage.size.height)];
        [self addSubview:imageView];
        
        if(self.name == @"Feed"){
            UIImage *optionImage = [ImageFactory createImageWithName:@"optionFeedPet" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:optionImage];
            [imageView setFrame:CGRectMake(100, 115, optionImage.size.width, optionImage.size.height)];
            [self addSubview:imageView];

        }else if(self.name == @"Clean"){
            UIImage *optionImage = [ImageFactory createImageWithName:@"optionCleanPet" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:optionImage];
            [imageView setFrame:CGRectMake(100, 107, optionImage.size.width, optionImage.size.height)];
            [self addSubview:imageView];
        }else if(self.name == @"Sleep"){
            UIImage *optionImage = [ImageFactory createImageWithName:@"optionSleepPet" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:optionImage];
            [imageView setFrame:CGRectMake(100, 115, optionImage.size.width, optionImage.size.height)];
            [self addSubview:imageView];
        }else if(self.name == @"Play"){
            UIImage *optionImage = [ImageFactory createImageWithName:@"optionPlayPet" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:optionImage];
            [imageView setFrame:CGRectMake(100, 115, optionImage.size.width, optionImage.size.height)];
            [self addSubview:imageView];
        }else if(self.name == @"Organize"){
            UIImage *optionImage = [ImageFactory createImageWithName:@"optionOrganizePet" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:optionImage];
            [imageView setFrame:CGRectMake(100, 115, optionImage.size.width, optionImage.size.height)];
            [self addSubview:imageView];
        }
    }
    return self;
}



@end
