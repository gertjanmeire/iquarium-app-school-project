//
//  AquariumElementView.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 12/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "AquariumElementView.h"

@implementation AquariumElementView

@synthesize appModel = _appModel;
@synthesize elementID = _elementID;
@synthesize xValue = _xValue;
@synthesize yValue = _yValue;
@synthesize imageView = _imageView;
@synthesize changePositionLabel = _changePositionLabel;
@synthesize xcoordLabel = _xcoordLabel;
@synthesize ycoordLabel = _ycoordLabel;
@synthesize xValueSlider = _xValueSlider;
@synthesize yValueSlider = _yValueSlider;
@synthesize imageName = _imageName;

- (id)initWithX:(NSNumber *)xValue andY:(NSNumber *)yValue andID:(NSNumber *)elementID andName:(NSString *)elementName
{
    self = [super init];
    if (self) {
        // Initialization code
        NSLog(@"[AquariumElementView] created with values: %d and %d", [xValue intValue], [yValue intValue]);
        NSLog(@"Element id: %d", [elementID intValue]);
        
        self.appModel = [AppModel sharedAppModel];
        
        self.elementID = [elementID intValue];
        self.xValue = xValue;
        self.yValue = yValue;
        self.imageName = elementName;
        
 
        //Element image view
        UIImage *elementImage = [ImageFactory createImageWithName:self.imageName andType:@"png" andDirectory:@"images"];
        self.imageView = [[UIImageView alloc] initWithImage:elementImage];
        [self.imageView setFrame:CGRectMake(0, 10, elementImage.size.width, elementImage.size.height)];
        [self addSubview:self.imageView];
        
        //Change position label
        UIImage *changePositionLabelImage = [ImageFactory createImageWithName:@"changePositionLabel" andType:@"png" andDirectory:@"images"];
        self.changePositionLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, elementImage.size.height + 25, changePositionLabelImage.size.width, changePositionLabelImage.size.height)];
        [self.changePositionLabel setBackgroundColor:[UIColor colorWithPatternImage:changePositionLabelImage]];
        [self addSubview:self.changePositionLabel];
        
        //X-coord label
        UIImage *xcoordLabelImage = [ImageFactory createImageWithName:@"xcoordLabel" andType:@"png" andDirectory:@"images"];
        self.xcoordLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, elementImage.size.height + 65, xcoordLabelImage.size.width, xcoordLabelImage.size.height)];
        [self.xcoordLabel setBackgroundColor:[UIColor colorWithPatternImage:xcoordLabelImage]];
        [self addSubview:self.xcoordLabel];

        //Y-coord label
        UIImage *ycoordLabelImage = [ImageFactory createImageWithName:@"ycoordLabel" andType:@"png" andDirectory:@"images"];
        self.ycoordLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, elementImage.size.height + 115, ycoordLabelImage.size.width, ycoordLabelImage.size.height)];
        [self.ycoordLabel setBackgroundColor:[UIColor colorWithPatternImage:ycoordLabelImage]];
        [self addSubview:self.ycoordLabel];
        
        //Slider images
        UIImage * sliderLeftTrackImage = [ImageFactory createImageWithName:@"sliderLeftTrack" andType:@"png" andDirectory:@"images"];
        UIImage * sliderRightTrackImage = [ImageFactory createImageWithName:@"sliderRightTrack" andType:@"png" andDirectory:@"images"];
        UIImage * thumbImage = [ImageFactory createImageWithName:@"thumb" andType:@"png" andDirectory:@"images"];
        
        //X-value slider
        self.xValueSlider = [[UISlider alloc] initWithFrame:CGRectMake(25, elementImage.size.height + 65, 200, 50)];
        [self.xValueSlider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
        [self.xValueSlider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
        [self.xValueSlider setThumbImage:thumbImage forState:UIControlStateNormal];
        self.xValueSlider.minimumValue = 0;
        self.xValueSlider.maximumValue = 1200;
        [self.xValueSlider setValue:[self.xValue intValue] animated:YES];
        [self.xValueSlider addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.xValueSlider];
        
        //Y-value slider
        self.yValueSlider = [[UISlider alloc] initWithFrame:CGRectMake(25, elementImage.size.height + 115, 200, 50)];
        [self.yValueSlider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
        [self.yValueSlider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
        [self.yValueSlider setThumbImage:thumbImage forState:UIControlStateNormal];
        self.yValueSlider.minimumValue = 0;
        self.yValueSlider.maximumValue = 900;
        [self.yValueSlider setValue:[self.yValue intValue] animated:YES];
        [self.yValueSlider addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.yValueSlider];
        
    }
    return self;
}

-(void)changeValue:(UISlider *)sender{
    if(sender == self.xValueSlider){
        NSLog(@"Change x value to %f for element %d", round(sender.value), self.elementID);
        [self.appModel changeElementPosition:round(sender.value) withOrientation:@"xpos" forElementWithID:self.elementID];
        
    }else if(sender == self.yValueSlider){
        NSLog(@"Change y value to %f for element %d", round(sender.value), self.elementID);
        [self.appModel changeElementPosition:round(sender.value) withOrientation:@"ypos" forElementWithID:self.elementID];

    }
}

@end
