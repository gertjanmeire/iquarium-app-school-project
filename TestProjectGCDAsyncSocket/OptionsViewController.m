//
//  OptionsViewController.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "OptionsViewController.h"

@interface OptionsViewController ()

@end

@implementation OptionsViewController

@synthesize appModel = appModel;
@synthesize optionsPicker = _optionsPicker;

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        NSLog(@"Main view controller initialised");
        
        //Set app model
        appModel = [[AppModel sharedAppModel] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];

    
    
    //Set the UIScrollView with all the user's pet's
    UIImage *optionsPickerImage = [ImageFactory createImageWithName:@"petpicker" andType:@"png" andDirectory:@"images"];
    self.optionsPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, optionsPickerImage.size.width, optionsPickerImage.size.height)];
    self.optionsPicker.backgroundColor = [UIColor colorWithPatternImage: optionsPickerImage];
    [self.optionsPicker setBounces:YES];
    [self.optionsPicker setPagingEnabled:YES];
    [self.optionsPicker setShowsHorizontalScrollIndicator:YES];
    [self.optionsPicker setCanCancelContentTouches:NO];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectOption)];
    [self.optionsPicker addGestureRecognizer:tapGesture];
    
    self.optionsPicker.delegate = self;
    [self.view addSubview:self.optionsPicker];
    
    
    NSArray *optionsArray = [NSArray arrayWithObjects:@"Feed", @"Clean", @"Sleep", @"Play", @"Organize", nil];
    
    //Insert pets in pet picker UIScrollView
    NSInteger optionsCount= optionsArray.count;
    
    for(int i = 0; i< optionsCount; i++) {
        CGFloat x = i * self.view.frame.size.width;
        OptionsView *optionsView = [[OptionsView alloc] initWithName:optionsArray[i]];
        [optionsView setFrame:CGRectMake(20+ x, 0,self.view.frame.size.width - 40, 216)];
        [self.optionsPicker addSubview:optionsView];
    }
    
    self.optionsPicker.contentSize = CGSizeMake(self.view.frame.size.width *optionsCount, 216);

    
    UIImage * logoutButtonImage = [ImageFactory createImageWithName:@"logoutButton" andType:@"png" andDirectory:@"images"];
    self.logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.logoutButton setFrame:CGRectMake(105, 225, logoutButtonImage.size.width, logoutButtonImage.size.height)];
    [self.logoutButton setImage:logoutButtonImage forState:UIControlStateNormal];
    [self.logoutButton addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.logoutButton];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES];

    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_options" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    [super viewWillAppear:animated];
}


-(void)selectOption{
    NSLog(@"Select an option");
    int optionIndex = (int)(self.optionsPicker.contentOffset.x / self.optionsPicker.frame.size.width);
    NSLog(@"Index of option: %d", optionIndex);

    if(optionIndex == 0){
        //Show feed screen
        FeedViewController *feedVC = [[FeedViewController alloc] init];
        [self.navigationController pushViewController:feedVC animated:YES];
    }else if(optionIndex == 1){
        //Show clean screen
        CleanViewController *cleanVC = [[CleanViewController alloc] init];
        [self.navigationController pushViewController:cleanVC animated:YES];
    }else if(optionIndex == 2){
        //Show sleep screen
        SleepViewController *sleepVC = [[SleepViewController alloc] init];
        [self.navigationController pushViewController:sleepVC animated:YES];
    }else if(optionIndex == 3){
        //Show play screen
        PlayViewController *playVC = [[PlayViewController alloc] init];
        [self.navigationController pushViewController:playVC animated:YES];
    }else if(optionIndex == 4){
        OrganizeViewController *organizeVC = [[OrganizeViewController alloc] init];
        [self.navigationController pushViewController:organizeVC animated:YES];
    }
        
}

-(void)logout:(UIButton *)sender{
    NSLog(@"Logout");
    //Send logout string to server
    //Optional -> depends if output program has to react when user does not use it's app anymore...???
    
    
    //Change view controller back to the main controller
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
