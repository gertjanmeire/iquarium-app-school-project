//
//  SleepViewController.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 08/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "SleepViewController.h"

@interface SleepViewController ()

@end

@implementation SleepViewController

@synthesize appModel = _appModel;
@synthesize isLightOn = _isLightOn;

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        self.appModel = [AppModel sharedAppModel];
        
        self.isLightOn = YES;
        
        NSLog(@"add gesture recognizer");
        UIView *clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [self.view addSubview:clearView];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeLights:)];
        tapRecognizer.numberOfTapsRequired = 1;
        [clearView addGestureRecognizer:tapRecognizer];
    }
    return self;
}

-(void)changeLights:(UITapGestureRecognizer *)sender{
        NSLog(@"Tap");
        if(self.isLightOn){
            [self.appModel turnLightOff:YES];
            self.isLightOn = NO;
            UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg_sleep2" andType:@"png" andDirectory:@"images"];
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
            [self.view addSubview:backgroundImageView];
        }else{
            [self.appModel turnLightOff:NO];
            self.isLightOn = YES;
            UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg_sleep1" andType:@"png" andDirectory:@"images"];
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
            [self.view addSubview:backgroundImageView];
        }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationItem setHidesBackButton:YES];
    
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg_sleep1" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
        
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem = navbarBackButton;

}

-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES];
    
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_sleep" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
