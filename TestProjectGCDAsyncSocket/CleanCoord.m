//
//  CleanCoord.m
//  iQuarium
//
//  Created by Gert-Jan Meire on 17/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "CleanCoord.h"

@implementation CleanCoord

@synthesize xCoord = _xCoord;
@synthesize yCoord = _yCoord;

- (id)initWithXCoord:(NSNumber *)xCoord andYCoord:(NSNumber *)yCoord
{
    self = [super init];
    if (self) {
        NSLog(@"CleanCoord initialised");
        self.xCoord = xCoord;
        self.yCoord = yCoord;
    }
    return self;
}

@end
