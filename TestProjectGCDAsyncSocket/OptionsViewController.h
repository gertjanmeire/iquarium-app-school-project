//
//  OptionsViewController.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppModel.h"
#import "ImageFactory.h"
#import "OptionsView.h"
#import "FeedViewController.h"
#import "CleanViewController.h"
#import "SleepViewController.h"
#import "PlayViewController.h"
#import "OrganizeViewController.h"

@interface OptionsViewController : UIViewController
{
    AppModel *appModel;
}

@property (strong, nonatomic) AppModel *appModel;
@property (strong, nonatomic) UIScrollView *optionsPicker;
@property (strong, nonatomic) UIButton *logoutButton;

@end
