//
//  PetTypeView.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 08/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageFactory.h"

@interface PetTypeView : UIView

@property (strong, nonatomic) NSString *name;

- (id)initWithName:(NSString *)name;

@end
