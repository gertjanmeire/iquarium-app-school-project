//
//  SelectPetViewController.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 01/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "SelectPetViewController.h"
#import "ImageFactory.h"

@interface SelectPetViewController ()

@end

@implementation SelectPetViewController

@synthesize appModel = appModel;
@synthesize selectPetView = _selectPetView;
@synthesize createPetView = _createPetView;
@synthesize petPicker = _petPicker;
@synthesize nameLabel = _nameLabel;
@synthesize nameTextField = _nameTextField;
@synthesize petTypePicker = _petTypePicker;
@synthesize createPetButton = _createPetButton;

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        [self.navigationItem setHidesBackButton:YES];
        
        //Set app model
        self.appModel = [AppModel sharedAppModel];
        
        //Listen for disconnect event when user clicks the Logout button
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectHandler:) name:@"DISCONNECTED" object:nil];
     
        //Listen for SELECTED_A_PET event
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectPet:) name:@"SELECTED_A_PET" object:nil];
        
    }
    return self;
}

-(void)getUserPets{
    NSLog(@"test");
    NSObject *userInfoObject = self.appModel.userInfo;
    NSString *user_id = [userInfoObject valueForKey:@"id"];
    NSLog(@"user id: %@", user_id);
        
    //Check webservice is user exists
    //If so change view else show error message
    NSString *URLString = [NSString stringWithFormat:@"http://%@:8888/amfphp-2.1/Amfphp/", self.appModel.IP];
    NSLog(@"URLString: %@", URLString);
    AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"ExDevService" method:@"getUserPets" arguments:[NSArray arrayWithObjects: user_id, nil]];
    m_remotingCall.delegate = self;
    [m_remotingCall start];
    NSLog(@"test na call");
}

- (void)remotingCallDidFinishLoading:(AMFRemotingCall *)remotingCall receivedObject:(NSArray *)arr
{
	NSLog(@"remoting call was successful. received data: %@", arr);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
	//NSLog(@"remoting call was successful. received data: %@", arr);
	//CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    
    NSInteger viewcount= arr.count;
    if(viewcount == 0){
        NSLog(@"No user pets...show creation screen");
        self.selectPetView.hidden = YES;
        self.createPetView.hidden = NO;
        
        UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_createpet" andType:@"png" andDirectory:@"images"];
        [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
        
    }else{
        self.createPetView.hidden = YES;
        self.selectPetView.hidden = NO;
        CGFloat i = 0;
        for (NSObject *pet in arr) {
            NSLog(@"Pet name: %@", [pet valueForKey:@"name"]);
            CGFloat x = i * self.view.frame.size.width;
            
            NSNumber *petID = (NSNumber *)[pet valueForKey:@"id"];
            NSString *petMood = [pet valueForKey:@"mood"];
            NSString *petName = [pet valueForKey:@"name"];
            NSNumber *petState = (NSNumber *)[pet valueForKey:@"state"];
            NSNumber *petType = (NSNumber *)[pet valueForKey:@"type"];
            NSNumber *petHP = (NSNumber *)[pet valueForKey:@"hp"];
            NSNumber *petTimeSinceEaten = (NSNumber *)[pet valueForKey:@"time_since_eaten"];
            NSNumber *petUserID = (NSNumber *)[pet valueForKey:@"user_id"];
            
            NSLog(@"time since eaten %@", petTimeSinceEaten);
            
            NSLog(@"Before petview type: %@", petType);
            
            //CUSTOM UIVIEWS AANMAKEN!! met properties in voor het pet!!
            PetView *petView = [[PetView alloc] initWithID:petID andMood:petMood andName:petName andState:petState andType:petType andPetHP:petHP andPetTimeSinceEaten:petTimeSinceEaten andUserID:petUserID];
            [petView setFrame:CGRectMake(20+ x, 0,self.view.frame.size.width - 40, 216)];
            [self.petPicker addSubview:petView];
            
            i++;
        }
        
        self.petPicker.contentSize = CGSizeMake(self.view.frame.size.width *viewcount, 216);
    }
    
       
}

- (void)remotingCall:(AMFRemotingCall *)remotingCall didFailWithError:(NSError *)error
{
	NSLog(@"remoting call failed with error %@", error);
	CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Get the user his pets (AMF call)
    [self getUserPets];
    
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    self.selectPetView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.view addSubview:self.selectPetView];
    
    //Set the UIScrollView with all the user's pet's
    UIImage *petPickerImage = [ImageFactory createImageWithName:@"petpicker" andType:@"png" andDirectory:@"images"];
    self.petPicker = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, petPickerImage.size.width, petPickerImage.size.height)];
    self.petPicker.backgroundColor = [UIColor colorWithPatternImage: petPickerImage];
    [self.petPicker setBounces:YES];
    [self.petPicker setPagingEnabled:YES];
    [self.petPicker setShowsHorizontalScrollIndicator:YES];
    self.petPicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.petPicker setCanCancelContentTouches:NO];

    self.petPicker.delegate = self;
    [self.selectPetView addSubview:self.petPicker];
    
    
    self.createPetView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.view addSubview:self.createPetView];

    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 35, 200, 20)];
    [self.nameLabel setBackgroundColor:[UIColor clearColor]];
    [self.nameLabel setTextColor:[UIColor colorWithRed:6 green:119 blue:144 alpha:255]];
    [self.nameLabel setText:@"Choose a name"];
    [self.createPetView addSubview:self.nameLabel];
    
    //Name textfield
    UIImage *nameFieldImage = [ImageFactory createImageWithName:@"usernameField" andType:@"png" andDirectory:@"images"];
    self.nameTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(30, 60, nameFieldImage.size.width, nameFieldImage.size.height)];
    [self.nameTextField setBackground:nameFieldImage];
    [self.nameTextField setDelegate:self];
    [self.createPetView addSubview:self.nameTextField];
    
    //Pet type picker
    //Set the UIScrollView with all the user's pet's
    UIImage *petTypePickerImage = [ImageFactory createImageWithName:@"pettypepicker" andType:@"png" andDirectory:@"images"];
    self.petTypePicker = [[UIScrollView alloc] initWithFrame:CGRectMake(30, 105, petTypePickerImage.size.width, petTypePickerImage.size.height)];
    self.petTypePicker.backgroundColor = [UIColor colorWithPatternImage: petTypePickerImage];
    [self.petTypePicker setBounces:YES];
    [self.petTypePicker setPagingEnabled:YES];
    [self.petTypePicker setShowsHorizontalScrollIndicator:YES];
    self.petTypePicker.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    [self.petTypePicker setCanCancelContentTouches:NO];
    
    self.petTypePicker.delegate = self;
    [self.createPetView addSubview:self.petTypePicker];
    
    NSArray *pettypes = [NSArray arrayWithObjects:@"piranha", @"fish", nil];
    
    NSInteger viewcount = pettypes.count;
    for (int i=0; i<viewcount; i++) {
        NSLog(@"Add pet type");
        CGFloat x = i * self.petTypePicker.frame.size.width;
        //CUSTOM UIVIEWS AANMAKEN!! met properties in voor het pet!!
        PetTypeView *petTypeView = [[PetTypeView alloc] initWithName:pettypes[i]];
        [petTypeView setFrame:CGRectMake(x, 0,self.view.frame.size.width, 129)];
        [self.petTypePicker addSubview:petTypeView];
    }
    
    self.petTypePicker.contentSize = CGSizeMake(self.petTypePicker.frame.size.width *viewcount, 129);

    //Registration button
    UIImage *createPetButtonImage = [ImageFactory createImageWithName:@"createPetButton" andType:@"png" andDirectory:@"images"];
    self.createPetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.createPetButton setImage:createPetButtonImage forState:UIControlStateNormal];
    self.createPetButton.frame = CGRectMake(32, 255, createPetButtonImage.size.width, createPetButtonImage.size.height);
    [self.createPetButton addTarget:self action:@selector(createPet:) forControlEvents:UIControlEventTouchUpInside];
    [self.createPetView addSubview:self.createPetButton];
   
}

-(void)createPet:(UIButton *)sender{
    if(sender == self.createPetButton){
        NSString *petName = self.nameTextField.text;
        NSLog(@"petName: %@", petName);

        if([petName isEqualToString:@""]){
            
        }else{
            NSLog(@"Create the pet and save it to the DB");
            NSNumber *petType = [NSNumber numberWithInt:1];
            NSObject *userInfoObject = self.appModel.userInfo;
            NSString *user_id = [userInfoObject valueForKey:@"id"];
            
            
            NSArray *createPetArguments = [NSArray arrayWithObjects:petName, petType, user_id, nil];
            
            NSString *URLString = [NSString stringWithFormat:@"http://%@:8888/amfphp-2.1/Amfphp/", self.appModel.IP];
             AMFRemotingCall *m_remotingCall = [[AMFRemotingCall alloc] initWithURL:[NSURL URLWithString:URLString] service:@"ExDevService" method:@"savePet" arguments:[NSArray arrayWithObjects: createPetArguments, nil]];
             [m_remotingCall start];
            
            self.createPetView.hidden = YES;
            self.selectPetView.hidden = NO;
            
            UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_selectpet" andType:@"png" andDirectory:@"images"];
            [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
            
            [self getUserPets];
        }
    }
}


-(void)selectPet:(NSNotification *)note{
    NSLog(@"[SelectPetVC] Select the pet");
    NSDictionary *petData = [note userInfo];
    NSLog(@"Chosen pet name: %@", [petData valueForKey:@"name"]);
    //Set the chosen pet info in the app model before sending it to the server
    
    //Start the output app through the server
    [self.appModel inputProgramJoined:petData];
    
    OptionsViewController *optionsVC = [[OptionsViewController alloc] init];
    [self.navigationController pushViewController:optionsVC animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_selectpet" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    [super viewWillAppear:animated];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
