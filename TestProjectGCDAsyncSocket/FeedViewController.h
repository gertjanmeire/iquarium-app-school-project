//
//  FeedViewController.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 28/11/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageFactory.h"
#import "AppModel.h"

@interface FeedViewController : UIViewController <UIAccelerometerDelegate>

@property (strong, nonatomic) AppModel *appModel;
@property (strong, nonatomic) UIAccelerometer *accelerometer;

@end
