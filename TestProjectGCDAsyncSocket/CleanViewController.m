//
//  CleanViewController.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "CleanViewController.h"


@interface CleanViewController ()

@end

@implementation CleanViewController

@synthesize appModel = _appModel;

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        self.appModel = [AppModel sharedAppModel];
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationItem setHidesBackButton:YES];
    
    UIImage *backgroundImage = [ImageFactory createImageWithName:@"bg" andType:@"png" andDirectory:@"images"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:backgroundImageView];
    
    UIImage *navbarImage = [ImageFactory createImageWithName:@"navbar_clean" andType:@"png" andDirectory:@"images"];
    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
    
    UIImage *backButtonImage = [ImageFactory createImageWithName:@"backButton" andType:@"png" andDirectory:@"images"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 5, backButtonImage.size.width, backButtonImage.size.height);
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *navbarBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem = navbarBackButton;
    
    UIImage *dirtMaskImage = [ImageFactory createImageWithName:@"dirtMask" andType:@"png" andDirectory:@"images"];
    WipeAwayView *mask = [[WipeAwayView alloc] initWithFrame:CGRectMake(0,0,320,510)];
	[mask newMaskWithColor:[UIColor colorWithPatternImage:dirtMaskImage] eraseSpeed:0.50];
	[self.view addSubview:mask];


}

-(void)backButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
