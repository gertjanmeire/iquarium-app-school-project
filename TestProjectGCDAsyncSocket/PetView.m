//
//  PetView.m
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import "PetView.h"

@implementation PetView

@synthesize petID = _petID;
@synthesize mood = _mood;
@synthesize name = _name;
@synthesize state = _state;
@synthesize type = _type;
@synthesize hp = _hp;
@synthesize time_since_eaten = _time_since_eaten;
@synthesize userID = _userID;


- (id)initWithID:(NSNumber *)petID andMood:(NSString *)mood andName:(NSString *)name andState:(NSNumber *)state andType:(NSNumber *)type andPetHP:(NSNumber *)petHP andPetTimeSinceEaten:(NSNumber *)petTimeSinceEaten andUserID:(NSNumber *)userID
{
    self = [super init];
    if (self) {
        // Initialization code
        self.petID = petID;
        self.mood = mood;
        self.name = name;
        self.state = state;
        self.type = type;
        self.hp = petHP;
        self.time_since_eaten = petTimeSinceEaten;
        self.userID = userID;
        
        NSLog(@"Inside PetView it's name and type are: %@ and %@", self.name, self.type);
       
        
        if([self.type doubleValue] == [[NSNumber numberWithInt:1] doubleValue]){
            NSLog(@"type = 1");
            UIImage *petImage = [ImageFactory createImageWithName:@"petType1" andType:@"png" andDirectory:@"images"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:petImage];
            [imageView setFrame:CGRectMake(75, 110, petImage.size.width, petImage.size.height)];
            [self addSubview:imageView];
        }else if(type == [NSNumber numberWithInt:2]){
            
        }
        
        UILabel *nameLabel = [[UILabel alloc] init];
        [nameLabel setFrame:CGRectMake(55, 20, 200, 40)];
        [nameLabel setText:[NSString stringWithFormat:@"Name: %@", self.name]];
        [nameLabel setTextColor:[UIColor whiteColor]];
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        [nameLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        [self addSubview:nameLabel];
        
        UILabel *moodLabel = [[UILabel alloc] init];
        [moodLabel setFrame:CGRectMake(55, 40, 200, 40)];
        [moodLabel setText:[NSString stringWithFormat:@"Mood: %@", self.mood]];
        [moodLabel setTextColor:[UIColor whiteColor]];
        [moodLabel setBackgroundColor:[UIColor clearColor]];
        [nameLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        [self addSubview:moodLabel];
        
        
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    NSLog(@"touch petview");
    
    NSDictionary *petInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.name, @"name", self.type, @"type", self.mood, @"mood", self.state, @"state", self.hp, @"hp", self.time_since_eaten, @"time_since_eaten", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SELECTED_A_PET" object:self userInfo:petInfo];
}


@end
