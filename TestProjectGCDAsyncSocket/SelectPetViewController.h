//
//  SelectPetViewController.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 01/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMFRemotingCall.h"
#import "AppModel.h"
#import "OptionsViewController.h"
#import "PetView.h"
#import "PetTypeView.h"
#import "CustomTextField.h"

@interface SelectPetViewController : UIViewController <AMFRemotingCallDelegate, UITextFieldDelegate>
{
    AppModel *appModel;
}

@property (strong, nonatomic) AppModel *appModel;

@property (strong, nonatomic) UIView *selectPetView;
@property (strong, nonatomic) UIScrollView *petPicker;

@property (strong, nonatomic) UIView *createPetView;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) CustomTextField *nameTextField;
@property (strong, nonatomic) UIScrollView *petTypePicker;
@property (strong, nonatomic) UIButton *createPetButton;


@end
