//
//  SleepViewController.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 08/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppModel.h"
#import "ImageFactory.h"

@interface SleepViewController : UIViewController

@property (strong, nonatomic) AppModel *appModel;
@property (nonatomic) BOOL isLightOn;

@end
