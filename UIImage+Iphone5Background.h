//
//  UIImage+Iphone5Background.h
//  TestProjectGCDAsyncSocket
//
//  Created by Gert-Jan Meire on 03/12/12.
//  Copyright (c) 2012 Gert-Jan Meire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Iphone5Background)

+ (UIImage*)imageNamedForDevice:(NSString*)name;

@end
